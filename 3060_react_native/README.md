# 3060_react_native

## Demo
- Please check the video 
```
https://drive.google.com/file/d/1HhP1fC0SXfdkiLlwbhupZfo2ktQE6HE0/view?usp=sharing
```
- I rather not showing the source code as it may affect the original mini program
- I can show you this clone in person.

## Description
- A React Native clone of 3060 WeChat Mini Program + UI improvement + animation

## Why I Clone It
- Just for fun and I also want to improve the UI

## Why this project is not on App Store ?
- The Company wants to write this in flutter
