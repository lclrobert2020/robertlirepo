# flutter map example app

## Description
- A simple flutter app showing location of different users
- A previous code test demo so it is not the final product/ not the way that I will structure a flutter app
- Using updated api served by my own server

## I DONT WANNA BUILD, HOW CAN I TRY?
- go to apk folder, there is a apk for you to test.
## Features

- offline storage of data using hive database and flutter_map
- clean and functional design of map UXUI
- unit and widget test included
- support i18n
- null safety

## To do list
- add MobX state management
- use MobX together with get_it to provide global storage and dependency injection
- use MobX store to separate UI and business logic 
- more test case
- refractor code for tidiness 

## Installation

Git clone the project to your local 

```sh
git clone <path>
cd <folder>
```

Set up the .env files using .env_example and test.env_example template
.env is used for the app
test.env is used for testing 

```sh
BASE_URL=url of the api
API_TOKEN=your token
```

install package by flutter pub get
```sh
flutter pub get
```

build and test your app with android studio or Xcode, or using 
```sh
flutter run
```

## Packages installed

| Plugin | pub |
| ------ | ------ |
|modal_bottom_sheet | https://pub.dev/packages/modal_bottom_sheet |
| easy_localization | https://pub.dev/packages/easy_localization |
| cached_network_image | https://pub.dev/packages/cached_network_image |
| flutter_dotenv | https://pub.dev/packages/flutter_dotenv |
| flutter_map| https://pub.dev/packages/flutter_map |
|flutter_map_marker_popup| https://pub.dev/packages/flutter_map_marker_popup |
|fluttertoast| https://pub.dev/packages/fluttertoast |
|dio| https://pub.dev/packages/dio |
|hive| https://pub.dev/packages/hive |
|hive_flutter| https://pub.dev/packages/hive_flutter |

## Suggested improvement
- use google map 
## License

MIT
