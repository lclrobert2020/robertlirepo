import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:map_example/models/person_model.dart';
import 'package:map_example/widgets/map_screen/person_card.dart';

void main() {
  const json = {
        "_id": "73dba524-2fb9-42e6-ae3b-e11c8205d0af",
        "name": {"last": "Wagner", "first": "Camacho"},
        "email": "camacho.wagner@undefined.com",
        "picture": "http://placehold.it/175x244",
        "location": {"latitude": 22.3417661, "longitude": 22.3417663}
      };
  testWidgets('finds a person card', (WidgetTester tester) async {
    // Build an App with a Text widget that displays the letter 'H'.
    var mockPerson = PersonModel.fromJson(json);
    var email = mockPerson.email!;
    var name = mockPerson.name?.toJson().values.join(" ");
    await tester.pumpWidget( MaterialApp(
      home: Scaffold(
        body: PersonCard(person: mockPerson),
      ),
    ));
    expect(find.text(name!),findsOneWidget);
    expect(find.text(email), findsOneWidget);
    expect(find.byType(CircleAvatar), findsOneWidget);
    expect(find.byType(Icon), findsOneWidget);
    final RenderConstrainedBox box = tester.renderObject(find.byType(Icon));
  });
}