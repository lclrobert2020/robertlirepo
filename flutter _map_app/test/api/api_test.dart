import 'package:collection/collection.dart';
import 'package:dio/dio.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:map_example/api/api.dart';
import 'package:http_mock_adapter/http_mock_adapter.dart';

Future<void> main() async {
  TestWidgetsFlutterBinding.ensureInitialized();
  await dotenv.load(fileName: "test.env");
  late Dio dio;
  late DioAdapter dioAdapter;
  Response<dynamic> response;
  group('env variable', () {
    test("should have env api token", () {
      var apiToken = dotenv.env['API_TOKEN'];
      expect(apiToken, "test");
    });

    test("should have base url", () {
      var baseUrl = dotenv.env['BASE_URL'];
      expect(baseUrl, "https://testing.com");
    });

    group("dio api test", () {
      final json = {
        "_id": "73dba524-2fb9-42e6-ae3b-e11c8205d0af",
        "name": {"last": "Wagner", "first": "Camacho"},
        "email": "camacho.wagner@undefined.com",
        "picture": "http://placehold.it/175x244",
        "location": {"latitude": 22.3417661, "longitude": 22.3417663}
      };

      setUp(() {
        dio = Api().dio;
        dioAdapter = DioAdapter(dio: dio);
      });
      test('should add api token to request and get data', () async {
        const route = '/get';
        dioAdapter.onGet(route, (server) => server.reply(200, json),
            headers: {"Authorization": "Bearer test"});
        response = await dio.get(route,
            options: Options(headers: {"requiresApiToken": true}));
        expect(response.statusCode, 200);
        expect(const DeepCollectionEquality().equals(response.data, json), true);
      });
    });
  });

}
