import 'package:map_example/models/person_model.dart';
import 'package:test/test.dart';
import 'package:collection/collection.dart';

void main() {
  test('Person Model should be able to accept json and return json', () {
    final json = {
      "_id": "73dba524-2fb9-42e6-ae3b-e11c8205d0af",
      "name": {"last": "Wagner", "first": "Camacho"},
      "email": "camacho.wagner@undefined.com",
      "picture": "http://placehold.it/175x244",
      "location": {"latitude": 22.3417661, "longitude": 22.3417663}
    };
    final model = PersonModel.fromJson(json);
    final returnedJson = model.toJson();
    expect(const DeepCollectionEquality().equals(returnedJson, json), true);
  });
  test('Person Model should be able to accept json with null latitude and longitude and return json', () {
    final json = {
      "_id": "73dba524-2fb9-42e6-ae3b-e11c8205d0af",
      "name": {"last": "Wagner", "first": "Camacho"},
      "email": "camacho.wagner@undefined.com",
      "picture": "http://placehold.it/175x244",
      "location": {"latitude": null, "longitude": null}
    };
    final model = PersonModel.fromJson(json);
    final returnedJson = model.toJson();
    expect(const DeepCollectionEquality().equals(returnedJson, json), true);
  });
  
}
