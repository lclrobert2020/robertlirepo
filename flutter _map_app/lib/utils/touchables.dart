import 'package:flutter/material.dart';

class TouchableOpacity extends StatefulWidget {
  final Widget child;
  final VoidCallback onTap;
  final Duration duration;
  final double opacity = 0.5;

  const TouchableOpacity(
      {Key? key,
      required this.child,
      required this.onTap,
      this.duration = const Duration(milliseconds: 50)})
      : super(key: key);

  @override
  _TouchableOpacityState createState() => _TouchableOpacityState();
}

class _TouchableOpacityState extends State<TouchableOpacity> {
  late bool isDown;

  @override
  void initState() {
    super.initState();
    setState(() => isDown = false);
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTapDown: (_) => setState(() => isDown = true),
      onTapUp: (_) => setState(() => isDown = false),
      onTapCancel: () => setState(() => isDown = false),
      onTap: widget.onTap,
      child: AnimatedOpacity(
        child: widget.child,
        duration: widget.duration,
        opacity: isDown ? widget.opacity : 1,
      ),
    );
  }
}

class TouchableScale extends StatefulWidget {
  final Widget child;
  final VoidCallback onTap;
  final Duration duration;
  final double opacity = 0.2;

  const TouchableScale(
      {Key? key,
      required this.child,
      required this.onTap,
      this.duration = const Duration(milliseconds: 150)})
      : super(key: key);

  @override
  State<TouchableScale> createState() => TouchableScaleState();
}

class TouchableScaleState extends State<TouchableScale> {
  double scale = 1.0;

  void _changeScale() {
    setState(() => scale = scale == 1.0 ? 0.95 : 1.0);
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTapDown: (_) => _changeScale(),
      onTapUp: (_) => _changeScale(),
      onTapCancel: () => _changeScale(),
      onTap: widget.onTap,
      child: AnimatedScale(
        scale: scale,
        child: widget.child,
        duration: widget.duration,
      ),
    );
  }
}
