import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:map_example/models/person_model.dart';
import 'package:latlong2/latlong.dart';
class CustomMarker extends Marker {
  final double lat;
  final double lng;
  final PersonModel person;

  CustomMarker({required this.lat, required this.lng, required this.person})
      : super(
          anchorPos: AnchorPos.align(AnchorAlign.top),
          point: LatLng(lat, lng),
          builder: (BuildContext ctx) => Icon(
            Icons.location_pin,
            color: Colors.orange.shade800,
            size: 35,
          ),
        );
}