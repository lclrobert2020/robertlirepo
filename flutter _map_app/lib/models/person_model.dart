/// _id : "73dba524-2fb9-42e6-ae3b-e11c8205d0af"
/// name : {"last":"Wagner","first":"Camacho"}
/// email : "camacho.wagner@undefined.com"
/// picture : "http://placehold.it/175x244"
/// location : {"latitude":22.3417661,"longitude":null}

class PersonModel {
  PersonModel({
    String? id,
    Name? name,
    String? email,
    String? picture,
    Location? location,
  }) {
    _id = id;
    _name = name;
    _email = email;
    _picture = picture;
    _location = location;
  }

  PersonModel.fromJson(dynamic json) {
    _id = json['_id'];
    _name = json['name'] != null ? Name.fromJson(json['name']) : null;
    _email = json['email'];
    _picture = json['picture'];
    _location =
        json['location'] != null ? Location.fromJson(json['location']) : null;
  }
  String? _id;
  Name? _name;
  String? _email;
  String? _picture;
  Location? _location;

  String? get id => _id;
  Name? get name => _name;
  String? get email => _email;
  String? get picture => _picture;
  Location? get location => _location;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['_id'] = _id;
    if (_name != null) {
      map['name'] = _name?.toJson();
    }
    map['email'] = _email;
    map['picture'] = _picture;
    if (_location != null) {
      map['location'] = _location?.toJson();
    }
    return map;
  }
}

/// latitude : 22.3417661
/// longitude : null

class Location {
  Location({
    double? latitude,
    double? longitude,
  }) {
    _latitude = latitude;
    _longitude = longitude;
  }

  Location.fromJson(dynamic json) {
    _latitude = json['latitude'];
    _longitude = json['longitude'];
  }
  
  double? _latitude;
  double? _longitude;

  double? get latitude => _latitude;
  double? get longitude => _longitude;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['latitude'] = _latitude;
    map['longitude'] = _longitude;
    return map;
  }
}

/// last : "Wagner"
/// first : "Camacho"

class Name {
  Name({
    String? last,
    String? first,
  }) {
    _last = last;
    _first = first;
  }

  Name.fromJson(dynamic json) {
    _last = json['last'];
    _first = json['first'];
  }
  String? _last;
  String? _first;

  String? get last => _last;
  String? get first => _first;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['last'] = _last;
    map['first'] = _first;
    return map;
  }
}
