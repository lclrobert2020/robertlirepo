import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:map_example/models/person_model.dart';
import 'package:map_example/utils/touchables.dart';

class PersonCard extends StatefulWidget {
  final PersonModel person;
  final bool showTrailing;
  final double height;
  final double width;
  final bool dense;
  final VoidCallback? onTapCallback;
  const PersonCard({
    Key? key,
    required this.person,
    this.showTrailing = true,
    this.height = 100,
    this.width = double.maxFinite,
    this.dense = false, this.onTapCallback
  }) : super(key: key);

  @override
  _PersonCardState createState() => _PersonCardState();
}

class _PersonCardState extends State<PersonCard> {
  bool showPlaceholder = false;

  void handleOnTap() {
    if(widget.onTapCallback !=null){widget.onTapCallback!(); return;}
    Navigator.of(context).pop({
      "lat": widget.person.location?.latitude,
      "lng": widget.person.location?.longitude,
      "person": widget.person
    });
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      child: SizedBox(
        height: widget.height,
        width: widget.width,
        child: InkWell(
          onTap: (widget.showTrailing &&
                      widget.person.location?.latitude != null &&
                      widget.person.location?.longitude != null) ? handleOnTap : null,
          child: Center(
            child: ListTile(
              leading: CircleAvatar(
                radius: widget.height * 0.3,
                backgroundColor: Theme.of(context).backgroundColor,
                backgroundImage: NetworkImage(widget.person.picture ?? ""),
                onBackgroundImageError: (_, $) => setState(() {
                  showPlaceholder = true;
                }),
                child: showPlaceholder
                    ? Text(widget.person.name?.first?[0].toUpperCase() ?? "NA")
                    : const SizedBox(),
              ),
              title: Text(widget.person.name?.toJson().values.join(" ") ??
                  "no_name".tr()),
              trailing: (widget.showTrailing &&
                      widget.person.location?.latitude != null &&
                      widget.person.location?.longitude != null)
                  ? TouchableOpacity(
                      onTap: handleOnTap,
                      child: Icon(
                        Icons.near_me,
                        color: Theme.of(context).toggleableActiveColor,
                      ),
                    )
                  : null,
              subtitle: widget.dense
                  ? FittedBox(
                      fit: BoxFit.contain,
                      child: Text(
                        widget.person.email ?? "no_email".tr(),
                      ))
                  : Text(
                    widget.person.email ?? "no_email".tr(),
                  ),
              dense: widget.dense,
            ),
          ),
        ),
      ),
    );
  }
}
