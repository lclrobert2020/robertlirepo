import 'dart:ui';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_map_marker_popup/flutter_map_marker_popup.dart';
import 'package:latlong2/latlong.dart';
import 'package:map_example/hive/person_box.dart';
import 'package:map_example/models/person_model.dart';
import 'package:map_example/services/person_service.dart';
import 'package:map_example/utils/cached_tile_provider.dart';
import 'package:map_example/utils/custom_marker.dart';
import 'package:map_example/utils/display_toast.dart';
import 'package:map_example/utils/touchables.dart';
import 'package:map_example/widgets/map_screen/person_card.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

class MapScreen extends StatefulWidget {
  const MapScreen({Key? key}) : super(key: key);

  @override
  _MapScreenState createState() => _MapScreenState();
}

class _MapScreenState extends State<MapScreen> with TickerProviderStateMixin {
  final PopupController _popupLayerController = PopupController();
  final LatLng _initialCenter = LatLng(22.3700556, 114.1535941);
  late final MapController _mapController;
  bool isFetching = false;
  List<PersonModel> personList = [];
  VoidCallback? notifyModalSheet;
  PersonModel? currentPerson;

  late final AnimationController _animationController = AnimationController(
    duration: const Duration(seconds: 1),
    vsync: this,
  );
  late final Animation<Offset> _offsetAnimation = Tween<Offset>(
    begin: const Offset(0.0, -1.5),
    end: Offset.zero,
  ).animate(CurvedAnimation(
    parent: _animationController,
    curve: Curves.bounceIn,
  ));

  void animatedMapMove(LatLng destLocation, double destZoom) {
    final _latTween = Tween<double>(
        begin: _mapController.center.latitude, end: destLocation.latitude);
    final _lngTween = Tween<double>(
        begin: _mapController.center.longitude, end: destLocation.longitude);
    final _zoomTween = Tween<double>(begin: _mapController.zoom, end: destZoom);

    var controller = AnimationController(
        duration: const Duration(milliseconds: 500), vsync: this);

    Animation<double> animation =
        CurvedAnimation(parent: controller, curve: Curves.fastOutSlowIn);

    controller.addListener(() {
      _mapController.move(
          LatLng(_latTween.evaluate(animation), _lngTween.evaluate(animation)),
          _zoomTween.evaluate(animation));
    });

    animation.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        controller.dispose();
      } else if (status == AnimationStatus.dismissed) {
        controller.dispose();
      }
    });
    controller.forward();
  }

  void showToastMessage(String msg) {
    DisplayToast.showToast(msg);
  }

  @override
  void initState() {
    super.initState();
    _mapController = MapController();
    fetchData();
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  Future<void> fetchData() async {
    if (isFetching) {
      return;
    }
    try {
      setState(() {
        showToastMessage("fetch_data".tr());
        isFetching = true;
        if (notifyModalSheet != null) notifyModalSheet!();
      });
      List<PersonModel> data = await PersonService().getPerson();
      await saveDataToBox(data);
      var map = await PersonBox().getPersonMap();
      var list =
          map.values.toList().map((e) => PersonModel.fromJson(e)).toList();
      setState(() {
        personList = list;
        showToastMessage("fetch_data_success".tr());
        isFetching = false;
        if (notifyModalSheet != null) notifyModalSheet!();
      });
    } catch (e) {
      showToastMessage("fetch_data_fail".tr());
      fetchLocalData();
    }
  }

  Future<void> fetchLocalData() async {
    try {
      var map = await PersonBox().getPersonMap();
      var list =
          map.values.toList().map((e) => PersonModel.fromJson(e)).toList();
      setState(() {
        showToastMessage("use_local_data_success".tr());
        personList = list;
        isFetching = false;
        if (notifyModalSheet != null) notifyModalSheet!();
      });
    } catch (e) {
      showToastMessage("fetch_data_fail".tr());
      PersonBox().clearBox();
    }
  }

  Future<void> saveDataToBox(List<PersonModel> personList) async {
    try {
      await PersonBox().putToBox(personList);
    } catch (e) {
      showToastMessage("save_to_db_fail".tr());
    }
  }

  void openModalSheet(context) {
    showBarModalBottomSheet(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(15), topRight: Radius.circular(15)),
      ),
      context: context,
      builder: (_context) {
        return getSheetContent();
      },
    ).then((value) {
      if (value["lat"] != null &&
          value["lng"] != null &&
          value["person"] != null) {
        setState(() {
          currentPerson = value["person"];
        });
        _animationController.forward();
        animatedMapMove(LatLng(value["lat"], value["lng"]), 13);
        _popupLayerController.showPopupsOnlyFor([
          CustomMarker(
              lat: value["lat"], lng: value["lng"], person: value["person"])
        ]);
      }
      notifyModalSheet = null;
    }).catchError((_) {
      notifyModalSheet = null;
    });
  }

  Widget getSheetContent() {
    return StatefulBuilder(
      builder: (context, builderSetState) {
        notifyModalSheet = () {
          builderSetState(() {});
        };
        return SizedBox(
          height: MediaQuery.of(context).size.height - 120,
          width: double.maxFinite,
          child: Stack(
            children: [
              Positioned(
                top: 0,
                left: 0,
                right: 0,
                height: 64,
                child: Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15),
                      color: Theme.of(context).primaryColor),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      TouchableOpacity(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Padding(
                          padding: const EdgeInsets.all(12.0),
                          child: Icon(
                            Icons.close,
                            size: 24,
                            color: Theme.of(context).iconTheme.color,
                          ),
                        ),
                      ),
                      TouchableOpacity(
                        onTap: () {
                          fetchData();
                        },
                        child: Padding(
                          padding: const EdgeInsets.all(12.0),
                          child: Icon(
                            Icons.refresh,
                            size: 24,
                            color: Theme.of(context).iconTheme.color,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
              Positioned(
                left: 0,
                right: 0,
                top: 64,
                bottom: 0,
                child: isFetching
                    ? const Center(child: CircularProgressIndicator())
                    : ListView.builder(
                        itemCount: personList.length,
                        itemBuilder: (context, index) {
                          return Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: PersonCard(
                              person: personList[index],
                            ),
                          );
                        },
                      ),
              ),
            ],
          ),
        );
      },
    );
  }

  List<Marker> getMarkers() {
    return personList.isNotEmpty
        ? personList.fold([], (previousValue, element) {
            if (element.location?.latitude != null &&
                element.location?.longitude != null) {
              previousValue.add(CustomMarker(
                  lat: element.location!.latitude!,
                  lng: element.location!.longitude!,
                  person: element));
            }
            return previousValue;
          })
        : [];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            FloatingActionButton(
              onPressed: () {
                var newLocale = context.locale.toString() == "en" ? "zh" : "en";
                context.setLocale(Locale(newLocale));
              },
              child: const Icon(Icons.translate),
            ),
            FloatingActionButton.extended(
                // ignore: prefer_const_constructors
                label: isFetching ? Text("loading").tr() : Text("menu").tr(),
                icon: isFetching
                    ? const Icon(Icons.download)
                    : const Icon(Icons.menu),
                onPressed: () => openModalSheet(context)),
          ],
        ),
      ),
      body: SizedBox(
        width: double.maxFinite,
        height: double.maxFinite,
        child: Stack(
          children: [
            Positioned(
              top: 0,
              left: 0,
              right: 0,
              bottom: 0,
              child: FlutterMap(
                mapController: _mapController,
                options: MapOptions(
                  maxZoom: 18,
                  minZoom: 8,
                  center: _initialCenter,
                  zoom: 13.0,
                ),
                children: [
                  TileLayerWidget(
                    options: TileLayerOptions(
                        urlTemplate:
                            "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
                        subdomains: ['a', 'b', 'c'],
                        tileProvider: const CachedTileProvider()),
                  ),
                  PopupMarkerLayerWidget(
                    options: PopupMarkerLayerOptions(
                      markerCenterAnimation: const MarkerCenterAnimation(),
                      markers: getMarkers(),
                      popupController: _popupLayerController,
                      popupAnimation: const PopupAnimation.fade(),
                      markerRotate: true,
                      markerRotateAlignment:
                          PopupMarkerLayerOptions.rotationAlignmentFor(
                              AnchorAlign.top),
                      popupBuilder: (_, Marker marker) {
                        return PersonCard(
                          person: (marker as CustomMarker).person,
                          width: 220,
                          height: 70,
                          showTrailing: false,
                          dense: true,
                        );
                      },
                    ),
                  )
                ],
              ),
            ),
            SlideTransition(
              position: _offsetAnimation,
              child: Container(
                color: Colors.transparent,
                width: double.maxFinite,
                padding:
                    EdgeInsets.only(top: MediaQuery.of(context).padding.top),
                height: MediaQuery.of(context).padding.top + 130,
                child: SizedBox(
                  height: 130,
                  width: double.infinity,
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: currentPerson != null
                        ? Stack(
                            children: [
                              Positioned(
                                  top: 25,
                                  left: 0,
                                  bottom: 0,
                                  right: 0,
                                  child: PersonCard(
                                    person: currentPerson!,
                                    onTapCallback: () => animatedMapMove(
                                        LatLng(
                                            currentPerson!.location!.latitude!,
                                            currentPerson!
                                                .location!.longitude!),
                                        13),
                                    dense: false,
                                  )),
                              Positioned(
                                left: -10,
                                top: -10,
                                width: 48,
                                height: 48,
                                child: TouchableOpacity(
                                  onTap: () {
                                    _animationController.reverse();
                                  },
                                  child: Center(
                                    child: Container(
                                      padding: const EdgeInsets.all(2),
                                      decoration: BoxDecoration(
                                        color:
                                            Theme.of(context).backgroundColor,
                                        borderRadius: BorderRadius.circular(20),
                                      ),
                                      child: const Icon(
                                        Icons.close,
                                        size: 18,
                                      ),
                                    ),
                                  ),
                                ),
                              )
                            ],
                          )
                        : null,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
