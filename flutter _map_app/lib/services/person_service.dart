import 'package:dio/dio.dart';
import 'package:map_example/api/api.dart';
import 'package:map_example/models/person_model.dart';

class PersonService {
  static PersonService? _instance;

  factory PersonService() => _instance ??= PersonService._();

  PersonService._();

  Future<List<PersonModel>> getPerson() async {
    var response = await Api().dio.get('/api/flutter-map-api',
        options: Options(headers: {"requiresApiToken": true}));
    List<PersonModel> list = List<PersonModel>.from(
        (response.data?["data"]["attributes"]?["data"] ?? [])
            .map((person) => PersonModel.fromJson(person)));
    return list;
  }
}
