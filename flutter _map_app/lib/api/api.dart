import 'package:dio/dio.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

class Api {
  final dio = createDio();

  Api._internal();

  static final _singleton = Api._internal();

  factory Api() => _singleton;

  static Dio createDio() {
    var dio = Dio(BaseOptions(
      baseUrl: dotenv.env['BASE_URL']??"",
      receiveTimeout: 10000, // 10 seconds
      connectTimeout: 10000,
      sendTimeout: 10000,
    ));

    dio.interceptors.addAll({
      AppInterceptors(dio),
    });
    return dio;
  }
}

class AppInterceptors extends Interceptor {
  final Dio dio;

  AppInterceptors(this.dio);

  @override
  void onRequest(
      RequestOptions options, RequestInterceptorHandler handler) async {
    if (options.headers.containsKey("requiresApiToken")) {
      options.headers.remove("requiresApiToken");
      var apiToken = dotenv.env['API_TOKEN'];
      if (apiToken == null || apiToken is! String) {
        var dioError = DioError(
            requestOptions: options,
            error: 'falied to get api token',
            type: DioErrorType.other);
        return handler.reject(dioError);
      } // throw error, can't get api token
      options.headers["Authorization"] = "Bearer " + apiToken;
    }
    return handler.next(options);
  }

  @override
  void onError(DioError err, ErrorInterceptorHandler handler) {
    return handler.next(err);
  }
}
