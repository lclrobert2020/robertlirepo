import 'package:hive/hive.dart';
import 'package:map_example/models/person_model.dart';

class PersonBox {
  PersonBox._privateConstructor();

  static final PersonBox _instance = PersonBox._privateConstructor();

  factory PersonBox() {
    return _instance;
  }

  final String boxName = "personBox";

  Box? _box;

  bool _isBoxOpen = false;

  get isBoxOpen => _isBoxOpen;

  get presentBox => _box;

  Future<Box> openBox() async {
    var box = await Hive.openBox('personBox');
    _box = box;
    _isBoxOpen = true;
    return box;
  }

  Future<void> clearBox() async {
    if (_box == null) {
      await openBox();
    }
    _box!.clear();
  }

  Future<void> putToBox(List<PersonModel> personList) async {
    if (_box == null) {
      await openBox();
    }
    for (var element in personList) {
      _box!.put(element.id, element.toJson());
    }
  }

  Future<Map<dynamic, dynamic>> getPersonMap() async {
    if (_box == null) {
      await openBox();
    }
    return _box!.toMap();
  }

  Future<void> closeBox() async {
    //according to doc, It is perfectly fine to leave a box open for the runtime of the app. If you need a box again in the future, just leave it open.
    if (_box == null) {
      await openBox();
    }
    _box!.close();
  }
}
