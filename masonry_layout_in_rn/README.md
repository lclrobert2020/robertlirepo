# masonry layour in react native ?

## Problem
- React native has a hard time to support masonry layout
- FlatList needs items on same row to have same height
- Wrapping two flex box around ScrollView seems ok, but how about performance when rendering large list of items?

## Recyclerlistview and masonary layout
- The recyclerlistview actually performs very well in large set of data because the use view recycling
- The offical RecyclerListView has no masonary layout support
- Someone written a masonary layout manager, but it has bugs
- Working example of masonary layout with bugs Already fixed by Robert Li
```
https://github.com/Flipkart/recyclerlistview/pull/526#issuecomment-1012718981
https://snack.expo.dev/@robertli93/grid_example
```



